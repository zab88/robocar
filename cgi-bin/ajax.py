# -*- encoding:utf-8 -*-
import cgi
import socket

print 'Content-Type: text/plain\n'
#contentType: "application/json; charset=utf-8",

#var_dump variables
vars = cgi.FieldStorage()

if 'what' in vars and 'action' in vars:
    what = vars['what'].value
    action = vars['action'].value

message = what+'-'+action
print(message)

#sending data via socket
host = socket.gethostname()
port = 6666
SKT = socket.socket()

SKT.connect((host, port))
SKT.send(message)

SKT.close()