# -*- coding:utf-8 -*-
#program should be alive
# server thread example http://www.binarytides.com/python-socket-server-code-example/
import robot
import socket
import sys

#HOST = ''   # Symbolic name, meaning all available interfaces
HOST = socket.gethostname()
PORT = 6666 # Arbitrary non-privileged port
RECV_BUFFER = 4096

# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s = socket.socket()
print 'Socket created'

#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
     
print 'Socket bind complete'

#Start listening on socket
s.listen(10)
print 'Socket now listening'

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])
    try:
        data = s.recv(RECV_BUFFER)
        if data:
            print data
    except:
        continue

s.close()


