import serial
from Tkinter import *
import time
import robot


def read_sonar(event):
    print(robot.read_sonar())


def read_temp(event):
    print(robot.read_temp())


def left(event):
    a = scale.get()
    a -= 5
    robot.move_servo_x(a)
    scale.set(a)


def right(event):
    a = scale.get()
    a += 5
    robot.move_servo_x(a)
    scale.set(a)


# set up GUI
root = Tk()
root.title("Control Panel")
# draw a nice big slider for servo position
scale = Scale(root,
              command=robot.move_servo_x,
              to=165,
              orient=HORIZONTAL,
              length=400,
              label='x')
scale.pack(anchor=CENTER)

scale1 = Scale(root,
               command=robot.move_servo_y,
               from_=40,
               to=175,
               orient=VERTICAL,
               length=400,
               label='y')
scale1.pack(side='right')


button = Button(root, text="Sonar", command=read_sonar)
button.pack(side='left')

button1 = Button(root, text="temp", command=read_temp)
button1.pack(side='left')

root.bind('<Key-n>', robot.set_default_servo_position())
root.bind('<Key-a>', robot.motor_left())
root.bind('<Key-d>', robot.motor_right())
root.bind('<Key-w>', robot.motor_forward())
root.bind('<Key-s>', robot.motor_backward())
root.bind('<Key-x>', robot.motor_stop())
# run Tk event loop
root.mainloop()
