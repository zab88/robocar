#!/bin/bash 

IP=`/sbin/ifconfig wlan0| sed -n 's/.*inet addr:\([0-9.]\+\)\s.*/\1/p'` 
wget "http://robot.lvla.ru/register/?name=rhrpb2&ip=$IP" -O /root/networks > /dev/null 

if [ $? != 0 ] 
then 
logger -t $0 "WiFi seems down, restarting"
ifdown —force wlan0 
ifup wlan0 
else 
cat /root/networks > /etc/wpa_supplicant/wpa_supplicant.conf
logger -t $0 "WiFi seems up." 
fi