import time
import BaseHTTPServer

import urlparse

import robot
robot = robot.Robot("ACM0", "rover")

HOST_NAME = ''
PORT_NUMBER = 8888

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        """Respond to a GET request."""
        print "got request ", self.path
        dd = urlparse.urlparse(self.path)
        data = urlparse.parse_qs(dd.query)
        # data = self.path[ self.path.find('?')+1:]
        what = data['what'][0]
        action = data['action'][0]
        #VEHICLE
        if what == "car-up":
            if action == "start":
                robot.motor_forward()
            elif action == "stop":
                robot.motor_stop()
        elif what == "car-down":
            if action == "start":
                robot.motor_backward()
            elif action == "stop":
                robot.motor_stop()
        elif what == "car-left":
            if action == "start":
                robot.motor_left()
            elif action == "stop":
                robot.motor_stop()
        elif what == "car-right":
            if action == "start":
                robot.motor_right()
            elif action == "stop":
                robot.motor_stop()
        #CAMERA
        elif what == "cam-down":
            if action == "start":
                robot.move_servo_down()
        elif what == "cam-up":
            if action == "start":
                robot.move_servo_up()
        elif what == "cam-left":
            if action == "start":
                robot.move_servo_left()
        elif what == "cam-right":
            if action == "start":
                robot.move_servo_right()

        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()

# self.wfile.write("<html><head><title>Title goes here.</title></head>")
#        self.wfile.write("<body><p>This is a test.</p>")
#        self.wfile.write("<p>You accessed path: %s</p>" % self.path)
#        self.wfile.write("</body></html>")

if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
