__author__ = 'kameron, Egor_Manylov'

import serial
import requests
import sys

class Robot(object):


    link = "http://robot.lvla.ru/telemetry/index.php"



    def __init__(self, port, name):
        is_windows = sys.platform.startswith('win')
        if is_windows:
            self.ser = serial.Serial('COM'+str(port), 115200)
        else:
            self.ser = serial.Serial('/dev/tty'+str(port), 115200)
        self.name=name
        self.CAM_Y=90
        self.CAM_X=90

    def move_servo_x(self, a):
        self.ser.write('s' + str(a)+ 'x')

    def move_servo_y(self, a):
        self.ser.write('s' + str(a) + 'y')

    def move_servo_left(self):
        self.CAM_X+=5
        self.move_servo_x(self.CAM_X)

    def move_servo_right(self):
        self.CAM_X-=5
        self.move_servo_x(self.CAM_X)

    def move_servo_up(self):
        self.CAM_Y+=5
        self.move_servo_y(self.CAM_Y)

    def move_servo_down(self):
        self.CAM_Y-=5
        self.move_servo_y(self.CAM_Y)


    def read_sonar(self, pos):
        self.ser.write('u1'+str(pos))
        return self.ser.readline()


    def read_temp(self):
        self.ser.write('t')
        return self.ser.readline()

    def read_hum(self):
        self.ser.write('h')
        return self.ser.readline()

    def set_default_servo_position(self):
        self.move_servo_y(40)
        self.move_servo_x(90)


    def motor_forward(self):
        self.ser.write('m250.0.')


    def motor_backward(self):
        self.ser.write('m-250.0.')


    def motor_right(self):
        self.ser.write('m0.250.')


    def motor_left(self):
        self.ser.write('m0.-250.')


    def motor_stop(self):
        self.ser.write('m0.0.')


    def send_data_to_server(self):
        try:
	    r = requests.post(self.link, params={"name": self.name, "temp": self.read_temp(), "humidity": self.read_hum()})
        except:
	    print ("Some error with request :(")
        if r.status_code == requests.codes.ok:
            print (str (r.text))
        else:
            print "Request error!"
