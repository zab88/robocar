#include <Servo.h>
#include <AFMotor.h>

#define F_Trig 5
#define F_Echo 3
#define L_Trig 13
#define L_Echo 12
#define R_Trig 11
#define R_Echo 8
#define TemPin 0

AF_DCMotor motor1(1);
AF_DCMotor motor2(2);

Servo servo_x;
Servo servo_y;
char a[1] = {0};
int angle,y,speedM,timeMS; 
float tempreture;

void setup() {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  pinMode(F_Trig, OUTPUT);
  pinMode(L_Trig, OUTPUT);
  pinMode(R_Trig, OUTPUT);
  pinMode(F_Echo, INPUT);
  pinMode(L_Echo, INPUT);
  pinMode(R_Echo, INPUT);
  
  servo_x.attach(9);  
  servo_y.attach(10);
  
  Serial.begin(115200);
  Serial.setTimeout(5);
  Serial.println("Started");
}

void loop() {
  while(Serial.available()) {
    a[0] = Serial.read();
    switch(a[0]) {
      case 's':
          angle = Serial.parseInt();
          a[3] = Serial.read();
          servoS(angle, a[3]);
          break;
      case 'u':
          angle = Serial.parseInt();
          a[2] = Serial.read();
          UltraSonic(a[2]);
          break;
      case 'm':
          speedM = Serial.parseInt();
          a[1] = Serial.read();
          timeMS = Serial.parseInt();
          motor(speedM, a[1], timeMS);
          break;
      case 't':
          temp();
          break;
    }
  }
}

void servoS(int angle, char axis) {
  if (axis == 'x') {
    servo_x.write(angle); }
  if (axis == 'y') {
    servo_y.write(angle); }
}

void UltraSonic(char side) {
  int Trig, Echo;
  unsigned int distance_sm = 0;
  unsigned int impulseTime = 0;
  switch(side) {
    case 'f':
      Trig = F_Trig;
      Echo = F_Echo;
      break;
    case 'l':
      Trig = L_Trig;
      Echo = L_Echo;
      break;
    case 'r':
      Trig = R_Trig;
      Echo = R_Echo;
      break;
  }
  digitalWrite(Trig, HIGH); 
  delayMicroseconds(10);
  digitalWrite(Trig, LOW); 
  impulseTime = pulseIn(Echo, HIGH);
  distance_sm = impulseTime/58;
  Serial.println(distance_sm);
}

void motor(int speedM, char arr, int time){
  if (arr == 's') {
    motor1.run(RELEASE);
    motor2.run(RELEASE);
  }
  
  if (arr == 'f') {
    motor1.run(FORWARD);
    motor2.run(FORWARD);
    motor1.setSpeed(speedM);
    motor2.setSpeed(speedM);
    delay(time);
  }
  if (arr == 'b') {
    motor1.run(BACKWARD);
    motor2.run(BACKWARD);
    motor1.setSpeed(speedM);
    motor2.setSpeed(speedM);
    delay(time);
  }
  
  if (arr == 'r') {
    motor1.run(BACKWARD);
    motor1.setSpeed(speedM);
    motor2.run(FORWARD);
    motor2.setSpeed(speedM);
    delay(time);
  }
  if (arr == 'l') {
    motor2.run(BACKWARD);
    motor2.setSpeed(speedM);
    motor1.run(FORWARD);
    motor1.setSpeed(speedM);
    delay(1000);
  }  
  motor(0, 's', 0);
}
    
void temp() {
  tempreture = analogRead(TemPin) * 5.0/1024 * 100;
  Serial.println(tempreture);
}
