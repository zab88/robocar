// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>
#include <Servo.h>

#define F_Trig 5
#define F_Echo 3
#define L_Trig 13
#define L_Echo 12
#define R_Trig 11
#define R_Echo 8

#define TemPin 0


AF_DCMotor motor1(1);
AF_DCMotor motor2(2);

Servo servo_x;
Servo servo_y;

/*
int angle;*/

void setup() {
  Serial.begin(115200); 
  Serial.println("Started!");

  // turn on motor
  motor1.setSpeed(200); 
  motor1.run(RELEASE);
  motor2.setSpeed(200); 
  motor2.run(RELEASE);
  
/*  //!!! This code breaks everything
  pinMode(F_Trig, OUTPUT);
  pinMode(L_Trig, OUTPUT);
  pinMode(R_Trig, OUTPUT);
  pinMode(F_Echo, INPUT);
  pinMode(L_Echo, INPUT);
  pinMode(R_Echo, INPUT);
*/
    
  servo_x.attach(9);  
  servo_y.attach(10);
}

char cmd, t;
int speedF, speedS;
int angle;
void loop() {
  if(Serial.available()) {
    cmd = Serial.read();
    switch(cmd) {
      case 's':
          angle = Serial.parseInt();
          t = Serial.read();
          controlServo(angle, t);
          Serial.println("servo control");
          break;
      //* does not work
      /*case 'u':
          angle = Serial.parseInt();
          t = Serial.read();
          UltraSonic(t);
          break;*/
      case 'm':          
          speedF = Serial.parseInt();
          t = Serial.read();
          speedS = Serial.parseInt();
          t = Serial.read();
          controlMotors(speedF, speedS);
          Serial.println("motor control");Serial.println(speedF);Serial.println(speedS);
          break;
      case 't':
          temp();
          break;
      default:
          Serial.print("unknown command: ");
          Serial.println(cmd);
          break;
    }
  }
}

void controlMotors(int speedFwd, int speedSide){
    int r = speedFwd + speedSide;
    int l = speedFwd - speedSide;

  if(r>0) { 
    motor1.run(FORWARD);    
    motor1.setSpeed(r);
  } else if(r<0) {
    motor1.run(BACKWARD);    
    motor1.setSpeed(-r);
  } else {
    motor1.run(RELEASE);    
  }
  
  if(l>0) { 
    motor2.run(FORWARD);    
    motor2.setSpeed(l);
  } else if(l<0) {
    motor2.run(BACKWARD);    
    motor2.setSpeed(-l);
  } else {
    motor2.run(RELEASE);    
  }
}

void controlServo(int angle, char axis) {
  if (axis == 'x') {
    servo_x.write(angle); 
  }
  if (axis == 'y') {
    servo_y.write(angle); 
  }
}

/* 
void UltraSonic(char side) {
  int Trig, Echo;
  unsigned int distance_sm = 0;
  unsigned int impulseTime = 0;
  switch(side) {
    case 'f':
      Trig = F_Trig;
      Echo = F_Echo;
      break;
    case 'l':
      Trig = L_Trig;
      Echo = L_Echo;
      break;
    case 'r':
      Trig = R_Trig;
      Echo = R_Echo;
      break;
  }
  digitalWrite(Trig, HIGH); 
  delayMicroseconds(10);
  digitalWrite(Trig, LOW); 
  impulseTime = pulseIn(Echo, HIGH);
  distance_sm = impulseTime/58;
  Serial.println(distance_sm);
}*/


void temp() {
  float tempreture = analogRead(TemPin) * 5.0/1024 * 100;
  Serial.println(tempreture);
}
