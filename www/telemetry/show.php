<?php

if ( !isset($_REQUEST['name']) ){
echo "no car name";
die();
}

$db = new SQLite3('../data/telemetry.db');


$results = $db->query('SELECT * FROM telemetry WHERE
                        name LIKE "zoomer" AND
                        (param LIKE "temp" OR param LIKE "humidity")
                        ORDER BY `time` DESC LIMIT 864'); //864=12*24*3
//AND time>"'.date('Y:m:d H:i:s', time()-86400*3).'"

$temperature = array();
$humidity = array();

while ($row = $results->fetchArray()) {
    if ( $row['param'] == 'temp' ){
        $temperature[] = array($row['time'], intval($row['value']) );
    }else if ($row['param'] == 'humidity'){
        $humidity[] = array($row['time'], intval($row['value']) );
    }
}
$temperature[] = array('Date', 'Temperature');
$temperature = array_reverse($temperature);

$humidity[] = array('Date', 'Humidity');
$humidity = array_reverse($humidity);
?>
<html>
<head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        google.setOnLoadCallback(drawChart2);
        function drawChart() {
            /*var data = google.visualization.arrayToDataTable([
                ['Year', 'Sales', 'Expenses'],
                ['2004',  1000,      400],
                ['2005',  1170,      460],
                ['2006',  660,       1120],
                ['2007',  1030,      540]
            ]);*/
            var data = google.visualization.arrayToDataTable(<?php echo json_encode($temperature); ?>);

            var options = {
                title: 'Zoomer temperature'
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
        function drawChart2() {
            var data = google.visualization.arrayToDataTable(<?php echo json_encode($humidity); ?>);
            var options = {
                title: 'Zoomer humidity'
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
            chart.draw(data, options);
        }
    </script>
</head>
<body>
<div id="chart_div" style="width: 1000px; height: 600px;"></div>
<br />
<br />
<br />
<div id="chart_div2" style="width: 1000px; height: 600px;"></div>
</body>
</html>