<?php

if (!isset($_REQUEST['name']) && !isset($_GET["create"]) && !isset($_GET["list"])){
    print json_encode(array("error"=>1));
	die();
}

$db = new SQLite3('../data/telemetry.db');

if (isset($_GET["create"])) {
    $sql_create = 'CREATE TABLE IF NOT EXISTS `telemetry` (
      `name` varchar(32) NOT NULL,
      `param` varchar(64) NOT NULL,
      `value` varchar(64) NOT NULL,
      `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    )';
//    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
	$db->query($sql_create);
    print json_encode(array("error"=>0));
	die();
}

if (isset($_REQUEST["name"])) {
    $params = $_REQUEST;
    unset($params['name']);
    foreach($params as $k=>$el){
        $db->query("insert into telemetry (name, param, value) values (
            '".SQLite3::escapeString($_REQUEST["name"])."',
            '".SQLite3::escapeString($k)."',
            '".SQLite3::escapeString($el)."')");
    }
    print json_encode(array("error"=>0));
	die();
}

$results = $db->query('SELECT * FROM telemetry ORDER BY `time` DESC LIMIT 20');
$result = array();
while ($row = $results->fetchArray()) {
    $result[]=$row;
}

print json_encode($result);
