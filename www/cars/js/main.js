jQuery(document).ready(function(){

    //direction select
    $('#direction').change(function(){
        var current_direction = $(this).val()
        //search all cities with given direction
        var found_cities = $.grep(ALL_CITIES, function(v) {
            return v.dir === current_direction// && v.age < 30;
        });

        //make list of cities
        var str_cities = []
        for (var c in found_cities){
            str_cities.push(found_cities[c].town)
        }

        $('.typeahead').typeahead('destroy')
        $('.typeahead').typeahead({
            source:str_cities,
            updater:function(data){
                setCity(data)
                return data;
            }
        })
    })


    $('#count_now').click(function(event){
        event.preventDefault()
        var stop = false;
        //$('.form-group').removeClass('alert-danger')
        clean_fields()

        if (!check_for_empty()) {
            return;
        }

        //search city
        var city_name = $('#cities').val()
        var found_cities = $.grep(ALL_CITIES, function(v) {
            return v.town == city_name
        });
        if (found_cities.length > 0){
            var city_object = found_cities[0]
        }else{
            var city_object = {
                'dir': $('#direction').val(),
                'town': $('#cities').val(),
                'region':'hz',
                'population': parseInt( $('#population').val() ),
                'gr_of_pop':'5-10',
                'count_of_POS':1,
                'age_rang': 1,
                m1: 1.0,
                m2: 1.0,
                m3: 1.0,
                a1: 70016.15,
                b1: 0.5047,
                a2: 76220.54,
                b2: 0.1756
            }
        }

        var X1 = 1.0 * $('#client_flow').val()
        if ( X1<0 || X1>1000.0 ){
            $('#client_flow').closest('.form-group').addClass('alert-danger')
            stop = true;
        }

        var X2 = parseInt( $('#competitors').val() )
        if (X2<0 || X2>30){
            $('#competitors').closest('.form-group').addClass('alert-danger')
            stop = true;
        }

        var X3 = parseInt( $('#ads').val() )
        if (X3 == 0){
            $('#ads').closest('.form-group').addClass('alert-danger')
            stop = true;
        }

        var X4 = false
        var where_vsp = $('#vsp_form').val()
        if (where_vsp=='0'){
            $('#vsp_form').closest('.form-group').addClass('alert-danger')
            stop = true;
        }else{
            var mm = {
                '0-5_1':3, '0-5_2':1, '0-5_3':2, '0-5_4':4,
                '1000+_1':1, '1000+_2':2, '1000+_3':3, '1000+_4':4,
                '100-500_1':1, '100-500_2':4, '100-500_3':2, '100-500_4':3,
                '10-30_1':1, '10-30_2':2, '10-30_3':3, '10-30_4':4,
                '30-50_1':2, '30-50_2':1, '30-50_3':3, '30-50_4':4,
                '500-1000_1':2, '500-1000_2':1, '500-1000_3':3, '500-1000_4':4,
                '50-100_1':2, '50-100_2':1, '50-100_3':3, '50-100_4':4,
                '5-10_1':2, '5-10_2':4, '5-10_3':3, '5-10_4':1
            }
            var kk = city_object.gr_of_pop + '_' + where_vsp;
            var X4 = mm[kk]
        }



        var X5 = $('#yfmr').val().replace(',', '.')
        X5 = 1.0 * X5
        if ( X5<0.75 || X5>4.0 || $('#yfmr').val()=='' ){
            $('#yfmr').closest('.form-group').addClass('alert-danger')
            stop = true;
        }else if (X5 > 2){
            X5 = ( 1. + X5/2. );
        }

        var X6 = parseInt( city_object.age_rang )

        var X7 = parseInt( $('#work_time').val() )
        if (X7 == 0){
            $('#work_time').closest('.form-group').addClass('alert-danger')
            stop = true;
        }

        var X8 = city_object.population / (city_object.count_of_POS+1.)

        var X9 = parseInt( $('#costs_rent').val() )
        var X10 = parseInt( $('#costs_ads').val() )
        var X11 = parseInt( $('#costs_salary').val() )

        if (!stop){
            calculate(city_object, X1, X2, X3, X4, X5, X6, X7, X8, X9, X10, X11)
        }
    })

    $('#reset1').click(function(event){
        clean_fields()
    })
})


function setCity(city_name){
    var found_city = $.grep(ALL_CITIES, function(v) {
        return v.town === city_name
    });
    found_city = found_city[0]
    $('#population').val(found_city.population)
    $('#count_of_pos').val(found_city.count_of_POS)
}

function clean_fields(){
    $('.form-horizontal')[1].reset()
    $('.form-group').removeClass('alert-danger')
}

function check_for_empty(){
    var res = true
    $('#form1 *').filter(':input').each(function(){
        var self = $(this)

        if ( self.val()=='' && self.prop("tagName")=='INPUT' ){
            self.closest('.form-group').addClass('alert-danger')
            res = false;
        }
        if ( self.val()=='0' && self.prop("tagName")=='SELECT' ){
            self.closest('.form-group').addClass('alert-danger')
            res = false;
        }
    });
    return res;
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}


//GENERAL FORMULA
function calculate(city_object, X1, X2, X3, X4, X5, X6, X7, X8, X9, X10, X11){
    var sranoe4 = 0.392348631825;
    console.log(city_object, X1, X2, X3, X4, X5, X6, X7, X8, X9, X10, X11)
    var Y1 = city_object.m1*( X1*0.0601 + X2*1.0471 + X3*3.7197 + X4*5.5906 + X5*33.1474 + X6*6.6177 + X7*2.0636 + X8*-0.2402 )
    var Y2 = city_object.m2*( X1*0.0323 + X2*0.2454 + X3*2.7445 + X4*3.0043 + X5*18.6895 + X6*4.7991 + X7*0.1149 + X8*-0.0605 )
    var Y3 = city_object.m3*( X1*0.0148 + X3*0.1057 + X3*1.8090 + X4*1.7255 + X5*8.68390 + X6*-3.755 + X7*1.3475 + X8*0.05200 )

    Y1 = Math.round(Y1)
    Y2 = Math.round(Y2)
    Y3 = Math.round(Y3)

    var Y4 = Y2*city_object.a1*city_object.b1 + (Y1-Y2)*city_object.a2*city_object.b2
    Y4 = Math.round(Y4/50000.)*50000

    var X5_ = 1.0 * $('#yfmr').val().replace(',', '.')
//    var X5_ = 2.
    var Y5 = ( Y4*sranoe4 - 3.0 * X9 - 3.9*X11*X5_ - 3.0 * X10 )/3. ;

    var Y6 = Math.round( ( 3.0*X9 + 3.0*X10 + 3.9*X11*X5_ )/sranoe4 );
    var Y7 = Math.round( ( 300000. + 3.0*X9 + 3.0*X10 + 3.9*X11*X5_ )/sranoe4 );
    var Y8 = Math.round( ( 500000. + 3.0*X9 + 3.0*X10 + 3.9*X11*X5_ )/sranoe4 );
    var Y9 = Math.round( ( 700000. + 3.0*X9 + 3.0*X10 + 3.9*X11*X5_ )/sranoe4 );


    Y5 = Math.round(Y5)
    Y6 = Math.round(Y6)
    Y7 = Math.round(Y7)
    Y8 = Math.round(Y8)
    Y9 = Math.round(Y9)

    $('#Y1').val( numberWithCommas(Y1) )
    $('#Y2').val( numberWithCommas(Y2) )
    $('#Y3').val( numberWithCommas(Y3) )
    $('#Y4').val( numberWithCommas(Y4) )
    $('#Y5').val( numberWithCommas(Y5) )
    $('#Y6').val( numberWithCommas(Y6) )
    $('#Y7').val( numberWithCommas(Y7) )
    $('#Y8').val( numberWithCommas(Y8) )
    $('#Y9').val( numberWithCommas(Y9) )
}




