<?php
$tabl = json_decode(file_get_contents("../data/devices"), true);
if (!$tabl) $tabl = array();
$tabl[] = array("name"=>$_GET["name"], "ip"=>$_GET["ip"], "time"=>mktime(), "address"=>$_SERVER["REMOTE_ADDR"]);
file_put_contents("../data/devices", json_encode($tabl));

print "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
";
$tabl = json_decode(file_get_contents("../data/networks"), true);
foreach ($tabl as $k=>$v) {
    if (empty( $v['password'])){
	print '
network={
    ssid="'.$v['name'].'"
    key_mgmt=NONE
    scan_ssid=1
}';
    }
    else
    {
 print '
network={
ssid="'.$v['name'].'"
psk="'.$v['password'].'"
}';
    }
}
