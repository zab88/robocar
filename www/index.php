<h1>Active devices</h1>
<table><tr><td>Time</td><td>Name</td><td>Address</td><td>Local</td></tr>
<?php
$tabl = json_decode(file_get_contents("data/devices"), true);
foreach ($tabl as $k=>$v) {
	print "<tr><td>".date("Y/m/d H:i:s",$v["time"])."</td><td>".$v["name"]."</td><td>".$v["address"]."</td><td>".$v["ip"]."</td></tr>";
}
?>
</table>
